import itertools
import webbrowser
import openpyxl
import scipy.misc
import pyautogui
import time

import numpy as np
import gurobipy as grb
import pandas as pd

#
# Fixed data
#

driver_names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']
line_names = ['Line 1', 'Line 2', 'Line 3']
all_shifts = range(14 * 2)
late_shifts = [t for t in range(14 * 2) if t % 2 == 1]
all_drivers = range(len(driver_names))
all_lines = range(len(line_names))
consec_3day_shifts = [all_shifts[i: i + 6] for i in xrange(0, len(all_shifts) - 5, 2)]
consec_4day_shifts = [all_shifts[i: i + 8] for i in xrange(0, len(all_shifts) - 7, 2)]
consec_4late_shifts = [late_shifts[i: i + 4] for i in xrange(0, len(late_shifts) - 3)]

#
# objective coeffs for MIP model
#

unassigned_shift_pen = -20.
late_early_pen = -30.
consec_4late_pen = -10.
late_shifts_d_pen = -8.
pref_shift_rwd = 3.
pref_dayoff_rwd = 4.
long_rest_rwd = 5.

#
# Short helper functions
#

day_s_to_s_ind = lambda d, s: (d - 1) * 2 + s


def main():
    t0 = time.time()
    print('Capturing screen...')
    screencap = pyautogui.screenshot()

    print('Parsing capture...')
    d_l, d_o, d_o_p, d_s_p, f_bg_xy = read_img(screencap)

    print('Solving')
    rdf, assis = solve_mip(d_l, d_o, d_o_p, d_s_p)

    print('Entering solution with mouse')
    t01 = time.time()
    write_mouse(assis, f_bg_xy)
    t1 = time.time()
    print('Time entering with mouse: %.02f' % (t1 - t01))

    t_min = int((t1 - t0) / 60)
    t_sec = round((t1 - t0) % 60)
    print('Finished in 00:%02i:%02i' % (t_min, t_sec))

    # # Alternative that takes spreadsheet input and writes to html
    # print('Reading')
    # d_l, d_o, d_o_p, d_s_p = read_data('in_att2.xlsx')
    # print('Solving')
    # rdf, assis = solve_mip(d_l, d_o, d_o_p, d_s_p)
    # rdf.to_html('out.html')
    # print('opening browser to solution...')
    # webbrowser.open('out.html')


def write_mouse(assignments, first_bg_xy):
    #
    # Locations in planning window of things
    #

    x_dist_shift = 31
    x_dist_day = 35
    y_dist_line = 30
    y_dist_driver = 37
    s0_x = 155
    l0_y = 61
    d0_y = 163

    move_time = 0.0
    pyautogui.PAUSE = 0.07
    click_time = 0.0

    # Click window
    pyautogui.moveTo(first_bg_xy[1] + 10, first_bg_xy[0] + 10, move_time, pyautogui.easeOutQuad)
    try:
        pyautogui.click(duration=click_time)
    except WindowsError:
        pass  # Workaround bug in pyautogui
    time.sleep(0.1)

    # Click assignments in
    for s, l, d in assignments:
        # Get click coords
        s_x = first_bg_xy[1] + s0_x + int(s / 2) * (x_dist_day + x_dist_shift) + (s % 2) * x_dist_shift
        l_y = first_bg_xy[0] + l0_y + l * y_dist_line
        d_y = first_bg_xy[0] + d0_y + d * y_dist_driver

        # Click line/shift
        pyautogui.moveTo(s_x, l_y, move_time, pyautogui.easeOutElastic)
        try:
            pyautogui.click(duration=click_time)
        except WindowsError:
            pass  # Workaround bug in pyautogui

        # Click driver/shift
        pyautogui.moveTo(s_x, d_y, move_time, pyautogui.easeOutElastic)
        try:
            pyautogui.click(duration=click_time)
        except WindowsError:
            pass  # Workaround bug in pyautogui


def read_img(img):
    #
    # Colours
    #

    border_rgb = [133, 143, 153]  # greyish border around planning window
    gridline_rgb = [105, 105, 105]  # Greyish gridlines in table
    background_rgb = [128, 128, 128]
    driver_sep_rgb = [160, 160, 160]
    min_driver_box_r = 225

    line1_rgb = [161, 193, 59]
    line2_rgb = [58, 180, 74]
    line3_rgb = [15, 105, 57]
    lines_rgb = [line1_rgb, line2_rgb, line3_rgb]

    dayoff_rgb = [1, 1, 1]
    pref_shift_rgb = [255, 255, 51]
    pref_dayoff_rgb = [153, 153, 153]

    #
    # Locations and spacing of data in plan_window
    #

    top_left_table_xy = (145, 138)
    start_dl_tl_xy = (145, 60)
    start_dl_tr_xy = (145, 105)
    table_cell_width = 66
    table_cell_height = 37

    #
    # Find the planning window
    #

    img_a = scipy.misc.fromimage(img)
    border_grey_xys = np.argwhere((img_a == border_rgb).all(axis=2))
    first_bg_xy = (border_grey_xys[:, 0].min(), border_grey_xys[:, 1].min())
    last_bg_xy = (border_grey_xys[:, 0].max(), border_grey_xys[:, 1].max())
    plan_window = img_a[first_bg_xy[0]:last_bg_xy[0], first_bg_xy[1]:last_bg_xy[1], :]
    assert (plan_window[top_left_table_xy] == gridline_rgb).all()
    assert plan_window[start_dl_tl_xy][0] > 200 and plan_window[start_dl_tr_xy][0] > 200

    #
    # Read driver line qualifications
    #

    curr_top_row = start_dl_tr_xy[0]
    left_col = start_dl_tl_xy[1]
    right_col = start_dl_tr_xy[1]

    # Iterate down image, effectively over drivers
    driver_lines_in = []
    while plan_window[curr_top_row, left_col, 0] > min_driver_box_r:
        driver_lines_in_d = []
        # Get bottom row by colour
        curr_bottom_row = curr_top_row + min(np.argwhere((plan_window[curr_top_row:, left_col, :] == driver_sep_rgb).all(axis=1)))

        # Check if line colours are in the area for each line
        for l in xrange(len(all_lines)):
            if len(np.argwhere((plan_window[curr_top_row:curr_bottom_row, left_col:right_col, :] == lines_rgb[l]).all(axis=2))) > 0:
                driver_lines_in_d.append(l + 1)

        # Append this driver's lines
        assert 0 < len(driver_lines_in_d) < 3
        driver_lines_in.append(driver_lines_in_d)

        # Get new curr_top_row
        curr_top_row = curr_bottom_row + 1

    assert len(driver_lines_in) == 11

    #
    # Read scheduling table
    #

    curr_row = top_left_table_xy[0] + 20
    left1 = top_left_table_xy[1] + 1
    right1 = top_left_table_xy[1] + table_cell_width - 1

    # Iterate down image
    driver_off_in = []
    driver_off_pref_in = []
    driver_shift_pref_in = []
    while (plan_window[curr_row, left1] != background_rgb).all():
        driver_off_in_d = []
        driver_off_pref_in_d = []
        driver_shift_pref_in_d = []

        # Iterate over days
        for dayi in xrange(14):
            curr_left = left1 + dayi * table_cell_width
            curr_right = right1 + dayi * table_cell_width

            # Check if driver off
            if (plan_window[curr_row, curr_left] == dayoff_rgb).all():
                driver_off_in_d.append(dayi + 1)

            # Check if driver has preference to be off
            if (plan_window[curr_row, curr_left] == pref_dayoff_rgb).all():
                driver_off_pref_in_d.append(dayi + 1)

            # Check if driver has AM shift preference
            if (plan_window[curr_row, curr_left] == pref_shift_rgb).all():
                driver_shift_pref_in_d.append((dayi + 1, 0))

            # Check if driver has PM shift preference
            if (plan_window[curr_row, curr_right] == pref_shift_rgb).all():
                driver_shift_pref_in_d.append((dayi + 1, 1))

        # Append to lists
        driver_off_in.append(driver_off_in_d)
        driver_off_pref_in.append(driver_off_pref_in_d)
        driver_shift_pref_in.append(driver_shift_pref_in_d)

        # Get new curr_row
        curr_row += table_cell_height

    assert len(driver_off_in) == 11
    assert len(driver_off_pref_in) == 11
    assert len(driver_shift_pref_in) == 11

    #
    # Transform
    #

    driver_lines_al = [line_als for line_als in driver_lines_in]
    driver_lines = []
    for d in all_drivers:
        disallow = []
        for l in all_lines:
            if l + 1 not in driver_lines_al[d]:
                disallow.append(l)
        driver_lines.append(disallow)

    driver_off = [[day_s_to_s_ind(day, shift) for day in days for shift in (0, 1)]
                  for days in driver_off_in]  # Indexed by driver, then list of shifts disallowed

    driver_off_pref = [days for days in driver_off_pref_in]  # Indexed by driver, then list of days undesired

    driver_shift_pref = [[day_s_to_s_ind(day, shift) for day, shift in day_shifts]
                         for day_shifts in driver_shift_pref_in]  # Indexed by driver, list of shifts desired

    return driver_lines, driver_off, driver_off_pref, driver_shift_pref, first_bg_xy


def read_data(fname):
    wb = openpyxl.load_workbook(filename=fname, read_only=True, data_only=True)

    lines_ws = wb['py_lines_in']
    driver_lines_in = []
    for d in xrange(1, 12):
        driver_lines_in_d = []
        for l in xrange(1, 4):
            if lines_ws.cell(row=d, column=l).value == 'y':
                driver_lines_in_d.append(l)
        driver_lines_in.append(driver_lines_in_d)

    off_ws = wb['py_off_in']
    driver_off_in = []
    for d in xrange(1, 12):
        driver_off_in_d = []
        for day in xrange(1, 15):
            if off_ws.cell(row=d, column=day).value == 'y':
                driver_off_in_d.append(day)
        driver_off_in.append(driver_off_in_d)

    off_pref_ws = wb['py_off_pref_in']
    driver_off_pref_in = []
    for d in xrange(1, 12):
        driver_off_pref_in_d = []
        for day in xrange(1, 15):
            if off_pref_ws.cell(row=d, column=day).value == 'y':
                driver_off_pref_in_d.append(day)
        driver_off_pref_in.append(driver_off_pref_in_d)

    shift_pref_ws = wb['py_shift_pref_in']
    driver_shift_pref_in = []
    for d in xrange(1, 12):
        driver_shift_pref_in_d = []
        for day in xrange(1, 15):
            if shift_pref_ws.cell(row=d, column=day).value == 'a':
                driver_shift_pref_in_d.append((day, 0))
            elif shift_pref_ws.cell(row=d, column=day).value == 'p':
                driver_shift_pref_in_d.append((day, 1))
        driver_shift_pref_in.append(driver_shift_pref_in_d)

    #
    # Transform
    #

    driver_lines_al = [line_als for line_als in driver_lines_in]
    driver_lines = []
    for d in all_drivers:
        disallow = []
        for l in all_lines:
            if l + 1 not in driver_lines_al[d]:
                disallow.append(l)
        driver_lines.append(disallow)

    driver_off = [[day_s_to_s_ind(day, shift) for day in days for shift in (0, 1)]
                  for days in driver_off_in]  # Indexed by driver, then list of shifts disallowed

    driver_off_pref = [days for days in driver_off_pref_in]  # Indexed by driver, then list of days undesired

    driver_shift_pref = [[day_s_to_s_ind(day, shift) for day, shift in day_shifts]
                         for day_shifts in driver_shift_pref_in]  # Indexed by driver, list of shifts desired

    return driver_lines, driver_off, driver_off_pref, driver_shift_pref


def sol_score(x_d, driver_off_pref, driver_shift_pref, print_step=True):
    """
    Independent solution scorer.
    """
    score = 0.0
    score0 = 0.0

    # Unassigned Shifts
    for l, s in itertools.product(all_lines, all_shifts):
        if round(sum(x_d[d, l, s].x for d in all_drivers)) != 1.:
            score -= 20
    if print_step:
        print('unassigned: %i' % (score - score0))
    score0 = score

    # Drivers cannot do late then early shift
    for d, s_0 in itertools.product(all_drivers, late_shifts[:-1]):
        if round(sum(x_d[d, l, s_0 + o].x for l in all_lines for o in (0, 1))) > 1.:
            score -= 30
    if print_step:
        print('late early: %i' % (score - score0))
    score0 = score

    # Maximum of 3 consecutive late shifts
    for d in all_drivers:
        late_shift_d = []
        for s in late_shifts:
            if round(sum(x_d[d, l, s].x for l in all_lines)) == 1.:
                late_shift_d.append(True)
            else:
                late_shift_d.append(False)
        consecs = [sum(1 for _ in group) for key, group in itertools.groupby(late_shift_d) if key]
        for consec in consecs:
            if consec > 3:
                score -= 10 * (consec - 3)
    if print_step:
        print('3 consec late: %i' % (score - score0))
    score0 = score

    # 4 late shifts per drive
    for d in all_drivers:
        if round(sum(x_d[d, l, s].x for l in all_lines for s in late_shifts)) != 4.:
            score -= 8 * abs(4 - sum(x_d[d, l, s].x for l in all_lines for s in late_shifts))
    if print_step:
        print('4late: %i' % (score - score0))
    score0 = score

    # Preferred days off
    for d in all_drivers:
        for day in driver_off_pref[d]:
            if round(sum(x_d[d, l, day_s_to_s_ind(day, sh)].x for l in all_lines for sh in (0, 1))) == 0.:
                score += 4
    if print_step:
        print('pref dayoff: %i' % (score - score0))
    score0 = score

    # Preferred shifts
    for d in all_drivers:
        for s in driver_shift_pref[d]:
            if round(sum(x_d[d, l, s].x for l in all_lines)) == 1.:
                score += 3
    if print_step:
        print('pref shifts: %i' % (score - score0))
    score0 = score

    # Long rests
    n_lr = 0
    for d in all_drivers:
        off_shift_d = []
        for s in all_shifts[:-1:2]:
            if round(sum(x_d[d, l, s + o].x for l in all_lines for o in (0, 1))) == 0.:
                off_shift_d.append(True)
            else:
                off_shift_d.append(False)
        consecs = np.array([sum(1 for _ in group) for key, group in itertools.groupby(off_shift_d) if key])
        n_lr += len(np.argwhere(consecs >= 3))
    score += n_lr * 5
    if print_step:
        print('longrests: %i' % (score - score0))

    if print_step:
        print('SCORE: %i' % score)

    return score


def solve_mip(driver_lines, driver_off, driver_off_pref, driver_shift_pref):
    """
    Main function to solve MIP model
    """

    model = grb.Model()
    model.params.threads = 4
    model.params.timeLimit = 5 * 60
    model.params.mipfocus = 0

    #
    # Variables
    #

    # Assignment variable: x[d, l, s] == 1 if driver d assigned to line l for shift s
    x = {(d, l, s): model.addVar(vtype=grb.GRB.BINARY, name="d%i on l%i for s%i" % (d, l, s))
         for d, l, s in itertools.product(all_drivers, all_lines, all_shifts)}

    consec_3day_sum = [[model.addVar(name="d%i 3daysum%i" % (d, i))
                        for i in xrange(len(consec_3day_shifts))] for d in all_drivers]

    # Also keep track of the 4day sum to make the rests over 3 days only worth one rest
    consec_4day_sum = [[model.addVar(name="d%i 4daysum%i" % (d, i))
                        for i in xrange(len(consec_4day_shifts))] for d in all_drivers]

    pref_dayoff_sum = {(d, day): model.addVar(name="d%i sumdayoff%i" % (d, day))
                       for d in all_drivers for day in driver_off_pref[d]}

    # Objective coefficient for sum of shift preferences respected is +pref_shift_rwd
    pref_shift_sum = [model.addVar(obj=pref_shift_rwd, name="d%i sumprefshift" % d) for d in all_drivers]

    ass_sum = model.addVar(name='sum assigned shifts')

    sum_late_early = {(d, late): model.addVar() for d in all_drivers for late in late_shifts[:-1]}

    consec_4late_sum = [[model.addVar(name='d%i 4latesum%i' % (d, i))
                         for i in xrange(len(consec_4late_shifts))]
                        for d in all_drivers]

    driver_late_sum = [model.addVar(name='d%i latesum' % d) for d in all_drivers]

    # Update model now all vars are defined
    model.update()

    #
    # Objective set up
    #

    # Set piecewise linear objective functions for consec_3day_sums
    for d in all_drivers:
        for i in xrange(len(consec_3day_shifts)):
            # +long_rest_rwd if 0 shifts assigned in 3day window, 0 if >=1 assigned
            model.setPWLObj(consec_3day_sum[d][i], [0, 1, 2, 3], [long_rest_rwd, 0, 0, 0])

    # Set PWL objective for consec_4day_sums
    for d in all_drivers:
        for i in xrange(len(consec_4day_shifts)):
            # -long_rest_rwd if 0 shifts assigned in 4day window, this will offset the extra 3day points
            # as for 4 day rest, 2 3day rests will be found, for 5 day rest, 2 4 day and 3 3 day rests will be found
            model.setPWLObj(consec_4day_sum[d][i], [0, 1, 2, 3, 4], [-long_rest_rwd, 0, 0, 0, 0])

    # PWL for preferred days off (Exact)
    for d in all_drivers:
        for day in driver_off_pref[d]:
            # +pref_dayoff_rwd if 0 shifts assigned out of the 2, 0 otherwise
            model.setPWLObj(pref_dayoff_sum[d, day], [0, 1, 2], [pref_dayoff_rwd, 0, 0])

    # PWL for unassigned shifts, gradient of -unassigned_shift_pen with f(shifts*lines)=0 (exact)
    model.setPWLObj(ass_sum, [0, len(all_shifts) * len(all_lines)],
                    [unassigned_shift_pen * len(all_shifts) * len(all_lines), 0])

    # PWL for late_earlys (exact)
    for d in all_drivers:
        for late in late_shifts[:-1]:
            # late_early_pen penalty for sum=2, 0 else
            model.setPWLObj(sum_late_early[d, late], [0, 1, 2], [0, 0, late_early_pen])

    # PWL for consec4lates (exact! if 5 consec, then there are two 4 consecs, 6 gives 3 and so on)
    for d in all_drivers:
        for i in xrange(len(consec_4late_shifts)):
            # consec_4late_pen penalty if sum=4, 0 else
            model.setPWLObj(consec_4late_sum[d][i], [0, 1, 2, 3, 4], [0, 0, 0, 0, consec_4late_pen])

    # PWL for driver late sum (exact)
    for d in all_drivers:
        # y = perdrivesum * abs(4 - x)
        model.setPWLObj(driver_late_sum[d], [3, 4, 5], [late_shifts_d_pen, 0, late_shifts_d_pen])

    # Maximising objectives
    model.setAttr('ModelSense', grb.GRB.MAXIMIZE)
    model.update()

    #
    # Constraints relating variables in obj
    #

    # Make 3 day sum be the sum of shifts in that 3day period
    for d in all_drivers:
        for i in xrange(len(consec_3day_shifts)):
            model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines for s in consec_3day_shifts[i])
                            == consec_3day_sum[d][i])

    # Make 4 day sum be the sum of shifts in that 4day period
    for d in all_drivers:
        for i in xrange(len(consec_4day_shifts)):
            model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines for s in consec_4day_shifts[i])
                            == consec_4day_sum[d][i])

    # pref_dayoff_sum for each d on each day is sum of the two shifts in the day
    for d in all_drivers:
        for day in driver_off_pref[d]:
            # Sum over all lines and the two shifts in the day is equal to pref_dayoff_sum
            model.addConstr(grb.quicksum(x[d, l, day_s_to_s_ind(day, s)] for l in all_lines for s in (0, 1))
                            == pref_dayoff_sum[d, day])

    # pref_shift_sum for each d is the sum over all lines and preffered shifts
    for d in all_drivers:
        model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines for s in driver_shift_pref[d])
                        == pref_shift_sum[d])

    # Assigned shifts is assigned shifts
    model.addConstr(grb.quicksum(x[d, l, s] for d in all_drivers for l in all_lines for s in all_shifts)
                    == ass_sum)

    # sum_late_early is sum of each late_early pair for each driver
    for d in all_drivers:
        for late in late_shifts[:-1]:
            model.addConstr(grb.quicksum(x[d, l, late + o] for l in all_lines for o in (0, 1))
                            == sum_late_early[d, late])

    # Make 4latesum be sum of shifts for driver over those 4lates
    for d in all_drivers:
        for i in xrange(len(consec_4late_shifts)):
            model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines for s in consec_4late_shifts[i])
                            == consec_4late_sum[d][i])

    # Make driver_late_sum be sum of driver's late shifts
    for d in all_drivers:
        model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines for s in late_shifts)
                        == driver_late_sum[d])

    #
    # 'Hard' Feasibility constraints
    #

    # Constraint enforcing that all lines and shifts have at most driver
    for l, s in itertools.product(all_lines, all_shifts):
        model.addConstr(grb.quicksum(x[d, l, s] for d in all_drivers) <= 1)

    # Constraint enforcing that a driver can only do one line in a shift
    for d, s in itertools.product(all_drivers, all_shifts):
        model.addConstr(grb.quicksum(x[d, l, s] for l in all_lines) <= 1)

    # No driver assigned on days off
    for d in all_drivers:
        model.addConstr(grb.quicksum(x[d, l, s] for s in driver_off[d] for l in all_lines) == 0)

    # Drivers cannot do lines they are not qualified on
    for d in all_drivers:
        model.addConstr(grb.quicksum(x[d, l, s] for s in all_shifts for l in driver_lines[d]) == 0)

    # Cannot do two shifts one day
    for d, s_0 in itertools.product(all_drivers, all_shifts[:-1:2]):
        model.addConstr(grb.quicksum(x[d, l, s_0 + o] for l in all_lines for o in (0, 1)) <= 1)

    model.write('mod.mps')

    # Optimize Model
    model.optimize()

    #
    # Deal with result
    #

    # Get independent score
    iscore = sol_score(x, driver_off_pref, driver_shift_pref, print_step=True)

    # Check independent score
    if np.equal(iscore, model.ObjVal):
        print('\n\n scores agree!')
    else:
        print('\n\n SCORES DO NOT AGREE...')

    # Get result
    resu = np.array([[[x[d, l, s].x for d in all_drivers] for l in all_lines] for s in all_shifts])

    # Array of tuples representing (shift, line, driver) assignments
    assignment_tups = np.argwhere(resu > 0.5)

    # Dictionary of assignments (assignment_d[d, s] = line_name)
    assignment_d = {(d, s): str(line_names[l]) for s, l, d in assignment_tups}

    # get array of line assigned to driver (row) i and shift (column) j
    def get_line_name(d, s):
        if (d, s) in assignment_d:
            return assignment_d[d, s]
        else:
            return ''
    assignment_a = np.array([[get_line_name(d, s) for s in all_shifts] for d in all_drivers])

    # Get results into dataframe to return
    cols = pd.MultiIndex.from_product([[str(i) for i in xrange(1, 15)], ['am', 'pm']], names=['day', 'shift'])
    df = pd.DataFrame(assignment_a, index=driver_names, columns=cols)

    return df, assignment_tups


if __name__ == '__main__':
    main()

# #
# # OLD ______
# #
#
# # Driver qualifications
# driver_lines_ind = {
#     'A': [3],
#     'B': [1],
#     'C': [2],
#     'D': [2],
#     'E': [2],
#     'F': [1, 2],
#     'G': [1, 2],
#     'H': [3],
#     'I': [3],
#     'J': [2, 3],
#     'K': [1, 3]
# }
#
# # Fixed days off
# driver_off_ind = {
#     'A': [1, 7, 8, 14],
#     'B': [3, 4, 10, 11],
#     'C': [2, 4, 9, 11],
#     'D': [1, 6, 8, 13],
#     'E': [5, 7, 12, 14],
#     'F': [3, 6, 10, 13],
#     'G': [1, 4, 8, 11],
#     'H': [2, 3, 9, 10],
#     'I': [3, 5, 10, 12],
#     'J': [1, 6, 8, 13],
#     'K': [2, 7, 9, 14]
# }
#
# # Preferred days off
# driver_off_pref_ind = {
#     'A': [3, 5, 11],
#     'B': [6, 14],
#     'C': [7, 13, 14],
#     'D': [4, 10, 11],
#     'E': [1, 9, 10],
#     'F': [1, 8],
#     'G': [6, 13, 14],
#     'H': [6, 13, 14],
#     'I': [8],
#     'J': [3, 4, 10, 11],
#     'K': [4, 5, 12]
# }
#
# # Preferred [(day, shift)] shift is 0, 1
# driver_shift_pref_ind = {
#     'A': [(4, 1), (9, 1), (12, 0)],
#     'B': [(7, 0), (13, 0)],
#     'C': [(8, 0), (12, 1)],
#     'D': [(2, 0), (3, 0), (12, 0)],
#     'E': [(2, 1), (4, 0)],
#     'F': [(5, 1), (9, 1), (14, 0)],
#     'G': [(2, 1), (4, 0), (7, 1)],
#     'H': [(4, 0), (11, 0)],
#     'I': [(11, 1), (13, 1)],
#     'J': [(2, 1), (7, 1), (9, 0), (12, 0)],
#     'K': [(1, 1)]
# }